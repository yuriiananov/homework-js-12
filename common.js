"use strict";

const buttons = document.querySelectorAll(".btn");

document.addEventListener("keydown", (event) => {
  const keyDown = event.key.toLowerCase();
  
  buttons.forEach(button => {
    const btnKey = button.dataset.key.toLowerCase();
    
    btnKey === keyDown ? button.style.background = "blue" : button.style.background = "black";
  });
});